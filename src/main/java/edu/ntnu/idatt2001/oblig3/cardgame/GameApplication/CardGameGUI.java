package edu.ntnu.idatt2001.oblig3.cardgame.GameApplication;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.io.IOException;

/**
 * this class is used for running the application, and has the main method that actually starts the program
 * @author Tomas Beranek
 */
public class CardGameGUI extends Application {


    /**
     * {@inheritDoc}
     * @param stage sthe starting stage
     * @throws IOException throws exception when path not found
     */
    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(CardGameGUI.class.getResource("/edu/ntnu/idatt2001/oblig3/cardgame/CardGame.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 600, 350);
        stage.setTitle("Hello!");
        stage.setScene(scene);
        stage.show();
    }

    /**
     * {@inheritDoc}
     * @param args
     */
    public static void main(String[] args) {
        launch();
    }
}