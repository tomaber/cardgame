package edu.ntnu.idatt2001.oblig3.cardgame.CardsFunctionalities;
import java.util.*;
import java.util.stream.Collectors;

/**
 *This class is used for all the methods needed to maintain a card deck.
 * It contains all the methods that are specified in the tasks ++.
 * The main function will be the constructor and the deal, and check hand methods.
 * @author Tomas Beranek
 */
public class DeckOfCards {
    List<PlayingCard> cardDeck;
    private final String[] suit = {"S","H","D","C"};
    private final int[] face = {1,2,3,4,5,6,7,8,9,10,11,12,13};
    private List<PlayingCard> cardsOnHand;
    private boolean hasFlush;

    /**
     *This is the DeckOfCards constructor where the new card deck is to be created when call on
     * This method streams through 4 different suit and for each stream it streams an additional 13 times.
     * the reason is that this will creat a new deck of 52 cards where the ace is 1 and up to 13 which is the king
     */
    public DeckOfCards() {
        cardDeck = new ArrayList<>();
        Arrays.stream(suit)
                .forEach(s -> Arrays.stream(face)
                        .forEach(f -> cardDeck.add(new PlayingCard(s, f))));
    }

    /**
     *this method deals 5 cards to the user/player. In the task it is unspecified how many cards there should be
     * but since most card games, and card combinations are based on 5 cards,
     * it didn't find it necessary to have more than 5
     * @return ArrayList with 5 playing cards, that the user will interact with
     */
    public ArrayList<PlayingCard> dealHand() {
        cardsOnHand = new ArrayList<>();

        cardDeck.stream().limit(5)
                .forEach(s -> cardsOnHand.add(cardDeck.get(new Random().nextInt(cardDeck.size()))));
        return (ArrayList<PlayingCard>) cardsOnHand;
    }

    /**
     *This method check if the players cards has a flush
     * it sorts all the cards and if the first and the last card is the same, then the player must have a flush
     * @param playerCards cards that the player have
     * @return a boolean dependent on if the player has or doesn't have a flush
     */
    public boolean checkForFlush(List<PlayingCard> playerCards){
        List<PlayingCard> FlushList;

        FlushList = playerCards.stream()
                .sorted(Comparator.comparing(PlayingCard::getSuit))
                .collect(Collectors.toList());
        hasFlush = FlushList.get(0).getSuit().equals(FlushList.get(4).getSuit());
        return hasFlush;
    }

    /**
     * This method computes the sum of all cards
     * @param playerCards players cards
     * @return int value of all the cards as sum
     */
    public int checkSum(List<PlayingCard> playerCards){
        return playerCards.stream().map(PlayingCard::getFace).reduce(0, Integer::sum);
    }

    /**
     * this method streams throughout player cards and looks for any match that equals queen of spades.
     * @param playingCards players cards
     * @return boolean depending on if the player has queen of spades
     */
    public boolean checkForQueenOfSpades(List<PlayingCard> playingCards){
        return playingCards.stream()
                .anyMatch(playingCard -> (playingCard.getSuit().equals("S") && (playingCard.getFace() == 12)));
    }

    /**
     *This method that collect all hearts cards and puts them in a arraylist
     * @param playingCards players cards
     * @return an arraylist of all playing card that are hearts
     */
    public ArrayList<PlayingCard> allCardsOfHearts(List<PlayingCard> playingCards){
        List<PlayingCard> allHeartsList;

        allHeartsList = playingCards.stream()
                .filter(cards -> cards.getSuit().equals("H")).collect(Collectors.toList());

        return (ArrayList<PlayingCard>) allHeartsList;
    }
}
