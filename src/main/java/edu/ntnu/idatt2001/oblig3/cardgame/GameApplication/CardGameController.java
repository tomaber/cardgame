package edu.ntnu.idatt2001.oblig3.cardgame.GameApplication;

import edu.ntnu.idatt2001.oblig3.cardgame.CardsFunctionalities.DeckOfCards;
import edu.ntnu.idatt2001.oblig3.cardgame.CardsFunctionalities.PlayingCard;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.text.Text;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * this is the controller class
 * it takes all information form the fxml file and transfers it to information, such as when buttons are pressed
 * then the information is sent to the other classes where the react to that information
 * @author Tomas Beranek
 */
public class CardGameController implements Initializable {

    @FXML public Text CardsOnHandLabel;
    @FXML private Label sumLabel;
    @FXML private Label flushLabel;
    @FXML private Label queenOfSpadesLabel;
    @FXML private Label allHeartsLabel;

    private DeckOfCards newDeck = new DeckOfCards();
    private ArrayList<PlayingCard> cardsOnHand = new ArrayList<>();


    /**
     * this method is in my case not needed because a make the starting scene in sceneries, and I therefore
     * don't need additional information or function when initialized
     * @param url The url to the scene
     * @param resourceBundle used for local specific objects
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }

    /**
     * this method is called when the user click on the deal hand button
     * this leads to creating a new deck of cards, and then drawing 5 cards which are then dealt to the user
     * as theirs cards
     */
    @FXML
    public void DealHandMethod() {
        newDeck = new DeckOfCards();
        cardsOnHand = newDeck.dealHand();
        CardsOnHandLabel.setText(cardsOnHand.toString());
    }

    /**
     * this method checks for all the different this the player can get from the cards
     * check flush queen of spades, hearts etc.
     * it then places the result in a label so that te user can see his results
     */
    @FXML
    public void CheckHandMethod() {
        flushLabel.setText(String.valueOf(newDeck.checkForFlush(cardsOnHand)));
        sumLabel.setText(String.valueOf(newDeck.checkSum(cardsOnHand)));
        queenOfSpadesLabel.setText(String.valueOf(newDeck.checkForQueenOfSpades(cardsOnHand)));
        allHeartsLabel.setText(String.valueOf(newDeck.allCardsOfHearts(cardsOnHand)));
    }
}
