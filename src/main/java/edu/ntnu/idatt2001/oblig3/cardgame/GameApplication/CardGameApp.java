package edu.ntnu.idatt2001.oblig3.cardgame.GameApplication;

/**
 * this is the starting page for the application
 * @author Tomas Beranek
 */
public class CardGameApp {

    /**
     * main method that starts the program, needed because a main that starts the program shouldn't inherit Application class
     * @param args
     */
    public static void main(String[] args) {
        CardGameGUI.main(args);
    }
}

