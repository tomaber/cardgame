module edu.ntnu.idatt2001.oblig3.cardgame {
    requires javafx.controls;
    requires javafx.fxml;


    exports edu.ntnu.idatt2001.oblig3.cardgame.GameApplication;
    opens edu.ntnu.idatt2001.oblig3.cardgame.GameApplication to javafx.fxml;
}